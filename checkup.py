import os
import whois
import yaml

def get_domain_expiration_date(domain_name):
    try:
        domain_info = whois.whois(domain_name)
        expiration_date = domain_info.expiration_date
        if isinstance(expiration_date, list):
            expiration_date = expiration_date[0]
        return expiration_date
    except Exception as e:
        return f"Ошибка: {e}"

directory_path = "/etc/nginx/sites-enabled/"  
output_file = "/root/domain_expiration_dates.yaml"

data = {}

for folder in os.listdir(directory_path):
    domain_name = folder.strip()  
    domain_path = os.path.join(directory_path, folder)
    
    if file_name.endswith(".conf"):
        domain_name = os.path.splitext(file_name)[0] 
        domain_path = os.path.join(directory_path, file_name)

        expiration_date = get_domain_expiration_date(domain_name)
        
            data[domain_name] = str(expiration_date)
            with open(domain_expiration_dates, 'w') as file:
                yaml.dump(data, file, default_flow_style=False)